#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string>


#ifndef __Bone__H
#define __Bone__H 1

class Bone
{
    public:
    Bone(int y, float minAngle, float maxAngle){startPosition.y = y; endPosition.y = y + 3; child = NULL; parent = NULL; this->minAngle = minAngle; this->maxAngle = maxAngle; length = endPosition.y  - startPosition.y; initialY = endPosition.y;}
    glm::vec2 startPosition;
    glm::vec2 endPosition;
    glm::mat4 modelMatrix;

    float length = 0;
    float angle = 0;
    float initialY = 0;

    float maxAngle = 360;
    float minAngle = 0;

    float getDesiredAngle();
    float angleGain = 0.1;

    Bone *parent;
    Bone *child;

    void calcAngle(int, int);
    operator std::string() const {  glm::to_string(startPosition.y) ; }


};
#endif
