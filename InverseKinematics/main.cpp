/* Inverse Kinematics Program by Naval Bhandari
    Keys to use:
    a - Add bone
    s - Use stiffness
    d - Use Damped Least Squares
    j - Use just pseudo-inverse
    + - Slow down
    - - Speed up

    Individual bones - Right click to select
    q - Increase length
    w - Decrease length
    e - Increase min/max angles
    r - Decrease min/max angles
    r - Increase stiffness
    t - Decrease stiffness
*/

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "Bone.h"

#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <math.h>

#include <vector>
#include <Eigen>

//Global Variables

#include <iostream>
#include <fstream>

Bone* boneParent;
Bone* endBone;
bool mouseClicked = false;

//Goal vector (where mouse was clicked)
Eigen::Vector3f G;
int bonesAmount;

//Slow in/out quantifier
int slowAmount = 0;
float defaultSlowAmount = 0;
int slowMult = 4;

float timeTotal = 0;
float startDist = 0;

Bone *selectedBone;

char method = 'j';

std::ofstream myfile;

static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //mat4 pMat = perspective(45.0f, 1.0f, 0.1f, 1500.0f);
    //gluPerspective(45.0f, ar, 0.1f, 1500.0f);
    //glMultMatrixf(value_ptr(pMat));
    glOrtho(-10,10, -10, 10, 0, 1);

    //mat4
    //glMultMatrixf(value_ptr(vMat));
}


//Simply draws a line from 0,0 to x,y parameters
static void drawLine(float x2, float y2)
{
    glBegin(GL_LINES);
        glVertex2f(0, 0);
        glVertex2f(x2, y2);
    glEnd();
}


// Function for calculating our angles use a variety of methods
static void calcAngles()
{
    //Check if the calculation is still delayed, if it is, decrements and pulls out of function.
    if (slowAmount > 0)
    {
        slowAmount --;
        return;
    }
     //Get our end position of our end bone and calculate the vector between the goal and end effector.
    Eigen::Vector3f E(endBone->endPosition.x, endBone->endPosition.y,0);
    Eigen::Vector3f V = G-E;
    //printf("Distance: %f ", V.lpNorm<1>());
    //Check if the distance between the goal and end effector is close enough, if it is, we've reached it and finishes calculating.
    float distance =V.lpNorm<1>();
    if (myfile.is_open())
    {
        myfile << (distance) <<  std::endl;
    }

    if (distance < 0.001)
    {
        mouseClicked = false;
        return;
    }
    //Calculate our delay based on the distance between the end effector and goal. Slower is farther away, until it reaches less than 1.
    defaultSlowAmount = distance < 1 ? 5 : distance * slowMult;
    //Resets the slow amount again.
    slowAmount = defaultSlowAmount;
//    if (defaultSlowAmount > 0) printf("Default slow time: %f \n", defaultSlowAmount);
    //Loop through each bone, create our Jacobian matrix.
    Eigen::MatrixXf J(3, bonesAmount);
    Bone *currentBone = boneParent;
    int i = 0;
    while (currentBone)
    {
        if (!currentBone)
            break;
        //Calculate cross product between the axis we're rotating around and vector from end effector to hinge on current bone, and fill Jacobian with x,y,z position.
        Eigen::Vector3f e = Eigen::Vector3f(0,0,1).cross(E- Eigen::Vector3f(currentBone->startPosition.x, currentBone->startPosition.y, 0));
        J(0,i) = e(0);
        J(1,i) = e(1);
        J(2,i) = e(2);
        currentBone = currentBone->child;
        i++;
    }
    // Calculate pseudo-inverse of Jacobian. Only do pseudoinverse if JI's determinant is not 0, so we know it the matrix is not singular, else use the transpose.
    Eigen::MatrixXf JI = (J * J.transpose()).inverse();
    float det = JI.determinant();
    Eigen::MatrixXf JT;
    if (!det)
        JT = J.transpose() * JI;
    else
        JT = J.transpose();
    //std::cout << JT << std::endl << std::endl;
   // std::cout << "OTHER:" << (J.transpose() * J).inverse() << std::endl << std::endl;

    Eigen::MatrixXf I(bonesAmount, bonesAmount);
    I.setIdentity();

    Eigen::VectorXf newTheta(bonesAmount);

    float delta;

    Eigen::VectorXf Theta = JT *V;
    //Check which method to use to calculate angles. Change effect of angle at which is does depending on method.
    switch (method)
    {
        case 'j':
            // Calculate angles using pseudo-inverse of Jacobian.
            newTheta = Theta;
            delta = 0.05;
            break;

        case 'd':
            //Damped least squares method. Chose arbitrary constant 0.05 for lambda^2.
            newTheta = ((J.transpose()*J + 0.05 * I).inverse() * J.transpose()) * V;
            delta = 0.5;
            break;
        case 's':
            //Change control method of pseudo-inverse by biasing angles more towards the midpoint of the minimum and maximum allowed angles for each bone.
            Eigen::MatrixXf JTJ = JT * J;
            Eigen::VectorXf z(bonesAmount);
            i = 0;
            currentBone = boneParent;
            //Loop through each bone, calculating the effect.
            while (currentBone)
            {
                if (!currentBone)
                    break;
                z(i) = currentBone->angleGain * (Theta(i) - currentBone->getDesiredAngle());
                currentBone = currentBone->child;
                i++;
            }

            int JTJSize = JTJ.rows();
            //Add the effect onto the pseudo inverse of Jacobian.
            newTheta = Theta + (JTJ - I) * z;
            delta = 0.001;
            break;
    }

    currentBone = boneParent;
    i = 0;
    //Apply angles to bone hinges.
    while (currentBone)
    {
        if (!currentBone)
            break;
        //Multiply the angle by a delta so it only adds a little bit and add it to the current angle.
        currentBone->angle += newTheta(i) * delta;
        //Clamp angle between min and max allowed angles.
        currentBone->angle = std::min(std::max(currentBone->minAngle, currentBone->angle), currentBone->maxAngle);
        currentBone = currentBone->child;
        i++;
    }
}

static void drawBone(Bone* bone)
{
    //Draw bones recursively.
//    printf("%s \n", glm::to_string(bone->position.y));
    glColor3f(0.0f, 1.0f, 0.0f);
    if (!bone) return;
    //Push current matrix on stack, so each bone draws relative from the previous one.
    glPushMatrix();
        //Get different between start and end position.
        float xDist = bone->endPosition.x - bone->startPosition.x;
        float yDist = bone->endPosition.y - bone->startPosition.y;
//        printf("xDist: %f \n yDist: %f, \n", xDist, yDist);
        //Do the transformations manually.
        glm::mat4 rotMatrix = glm::rotate(bone->modelMatrix, bone->angle, 0.0f, 0.0f, 1.0f) ;
        glm::mat4 transMatrix = glm::translate(rotMatrix, xDist, yDist, 0.0f);
        //Draw a green point at the start of the bone.
        glBegin(GL_POINTS);
            //If it's currently selected, draw it blue.
            if (bone == selectedBone)
                glColor3f(0.0f, 0.0f, 1.0f);
            glVertex2f(0, 0);
            glColor3f(0.0f, 1.0f, 0.0f);
        glEnd();
        //Rotate around the angle and draw a line from it
        glRotatef(bone->angle, 0,0,1);
        drawLine(0, bone->length);
        glTranslatef(0,bone->length,0);
        GLfloat modelMatrix[16];
        //Calculate new positions from transformations.
        glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix);
        bone->endPosition.x  = modelMatrix[12]; //transMatrix[3].x;
        bone->endPosition.y = modelMatrix[13]; //transMatrix[3].x;
        //glm::vec2 pos(bone->endPosition.x - bone->startPosition.x, bone->endPosition.y - bone->startPosition.y);
        //drawLine(pos.x, pos.y);
        //glm::mat4 transMatrix = glm::translate(bone->modelMatrix, pos.x, pos.y, 0.0f);
        //glTranslatef(pos.x, pos.y, 0);
        if (bone->child != NULL)
        {
            glm::vec2 pos(xDist, yDist);
            //drawLine(pos.x, pos.y);
            glm::mat4 transMatrix = glm::translate(bone->modelMatrix, pos.x, pos.y, 0.0f);
            //glTranslatef(pos.x, pos.y, 0);
            //Next bone's start position is this one's end.
            bone->child->modelMatrix = transMatrix;
            bone->child->startPosition = bone->endPosition;
            //Draw the next one.
            drawBone(bone->child);
        }
    glPopMatrix();
}


//Display callback method.
static void display(void)
{
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glColor3f(0,0,0);
    glLoadIdentity();
    //Checks if the mouse is clicked, if it is, calculate the angles, and then draw the bones.
    if (mouseClicked)
        calcAngles();
    drawBone(boneParent);
    //Draw the clicked point in red.
    glPointSize(10.0f);
    glBegin(GL_POINTS);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(G(0), G(1));
    glEnd();
    glutSwapBuffers();
}

static void mouse(int button, int state, int x, int y)
{
    //Get position of mouse in world coordinates.
    float xx = (float) (x - 320) / 32.0f;
    float yy = -(float) (y - 240) / 24.0f;
    if (button == GLUT_LEFT_BUTTON)
    {
        //Set the goal.
        mouseClicked = true;
        G = Eigen::Vector3f(xx, yy, 0);
        Eigen::Vector3f E(endBone->endPosition.x, endBone->endPosition.y,0);
        Eigen::Vector3f V = G-E;
        startDist =V.lpNorm<1>();
        slowAmount = startDist;
    }
    else if (button == GLUT_RIGHT_BUTTON)
    {
        //Select a hinge/bone by checking the distance between them.
        Bone *currBone = boneParent;
        while (currBone)
        {
            Eigen::Vector3f point = Eigen::Vector3f(xx, yy, 0);
            Eigen::Vector3f bonePos = Eigen::Vector3f(currBone->startPosition.x, currBone->startPosition.y, 0);
            Eigen::Vector3f V = bonePos - point;
            if (V.lpNorm<1>() < 0.5)
            {
                selectedBone = currBone;
                return;
            }
            currBone = currBone->child;
        }
    }
}



static void mouseMove(int x, int y)
{
//    int xx = (x - 320) / 32;
//    int yy = -((y - 240) / 24);

 }


void keyboard ( unsigned char key, int x, int y )  // Create Keyboard Function
{
  Bone *bone;
  //Keyboard functions to change variables.
  switch ( key )
  {
    //Esc key to exit.
    case 27:
        exit ( 0 );
        break;
    // a to add a new bone.
    case 'a':
        bone = new Bone(endBone->initialY+3, -100, 100);
        endBone->child = bone;
        bone->parent = endBone;
        endBone = endBone->child;
        bonesAmount++;
        break;
    //To do stiffness calculations with Jacobian psueodo inverse
    case 's':
        method = 's';
        break;
    //Use damp least squared method
    case 'd':
        method = 'd';
        break;
    //Use pseudo-inverse
    case 'j':
        method = 'j';
        break;
    //Make it slower;
    case '+':
        slowMult ++;
        break;
    //Make it faster;
    case '-':
        slowMult = slowMult > 0 ? slowMult - 1 : 0;
        break;
    default:
      break;
  }
  //Keys to change properties of selected bone.
  if (!selectedBone)
    return;
  switch ( key ) {
    //Increase length of bone
	case 'q':
	    selectedBone->length ++;
        break;
    //Decrease length
    case 'w':
        selectedBone->length --;
        break;
    //Increase max angles (2 at a time)
    case 'e':
        selectedBone->minAngle --;
        selectedBone->maxAngle ++;
        break;
    //Decrease min angles (2 at a time)
    case 'r':
        selectedBone->minAngle++;
        selectedBone->maxAngle--;
        break;
    //Change stiffness of bone.
    case 't':
        selectedBone->angleGain += 0.05;
        break;
    case 'y':
        selectedBone->angleGain -= 0.05;
        break;
  }


   glutPostRedisplay(); //request display() call ASAP
}

static void idle(void)
{
    glutPostRedisplay();
}

void onexit()
{
    myfile.close();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(100,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Inverse Kinematics - Naval");



    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMove);
    glutIdleFunc(idle);

    myfile.open("IK.txt");

    atexit(onexit);

    glutKeyboardFunc(keyboard);

    glClearColor(1,1,1,1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //Initialise bones
    bonesAmount = 6;
    boneParent = new Bone(0, -360, 360);
    Bone* prevBone = boneParent;
    //Each bone is 3 above the previous
    for (int i =1; i< bonesAmount; i++)
    {
        Bone *bone = new Bone(i*3, -100, 100);
        prevBone->child = bone;
        bone->parent = prevBone;
        prevBone = bone;
    }

    endBone = prevBone;

//    Bone* currentBone = boneParent;
//
//    while (currentBone)
//    {
//        if (!currentBone)
//            break;
//        currentBone = currentBone->child;
//    }

    glutMainLoop();

    return EXIT_SUCCESS;
}
