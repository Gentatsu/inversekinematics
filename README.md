    Inverse Kinematics Program by Naval Bhandari
    Keys to use:
    a - Add bone
    s - Use stiffness
    d - Use Damped Least Squares
    j - Use just pseudo-inverse
    + - Slow down
    - - Speed up

    Individual bones - Right click to select
    q - Increase length
    w - Decrease length
    e - Increase min/max angles
    r - Decrease min/max angles
    r - Increase stiffness
    t - Decrease stiffness