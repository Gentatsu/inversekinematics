\documentclass[a4, 11pt]{article}
\usepackage{fancyhdr}
\pagestyle{plain}
%\fancyhf{}
\usepackage{amssymb}
\def \stab { \qquad  \;}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{caption}
%\usepackage{subcaption}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage[toc,page]{appendix}
\usepackage{textcomp}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{titlesec}
\usepackage{todonotes}
\usepackage{pifont}
\usepackage{tabu}
\usepackage{cleveref}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage[font=small,skip=5pt]{caption}
\usepackage{adjustbox}
\usepackage{tabularx}
\usepackage{subfig}

\renewcommand{\thefootnote}{\arabic{footnote}}

\newcommand{\squeezeup}{\vspace{-2.5mm}}


\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
            {-2.5ex\@plus -1ex \@minus -.25ex}%
            {1.25ex \@plus .25ex}%
            {\normalfont\normalsize\bfseries}}
\makeatother

\setcounter{secnumdepth}{5} % how many sectioning levels to assign numbers to
\setcounter{tocdepth}{5}    % how many sectioning levels to show in ToC

\numberwithin{figure}{section}


\setlength{\textfloatsep}{2pt plus 1.0pt minus 2.0pt}


\begin{document}
\begin{figure}[H]
\centering
\begin{tabular}{|l |l|}
\hline
\textbf{Unit:} & Computer Animation and Games 1 \\ \hline
\textbf{Unit Code:} & CM50244 \\  \hline
\textbf{Coursework:} & 1 \\ \hline
\textbf{Module Leader(s): }& Yongliang Yang, Andrew Chinery \\ \hline
\textbf{Name:} & Naval Bhandari \\ \hline
\textbf{Student Id:} & 159407956 \\  \hline
\textbf{Username:} & nb615 \\ \hline
\textbf{Degree Programme:} & EngD Digital Entertainment \\ \hline
\end{tabular}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics{IK.png}
\end{figure}
\newpage
\section{Introduction}
In this report, we explore the use of inverse kinematics in 2D animation. Inverse kinematics is used to model realistic movement of moving an end appendage to a goal position, which is achieved by specifying a position in space and calculating how each appendage would rotate to get to the desired position. One example of using inverse kinematics in animation is when an animator wants to move a person's foot forward. By using inverse kinematics, the foot can be moved, and the rest of the body will adjust, so that it doesn't separate from the foot. Inverse kinematics is also highly used in robotics, as cameras are generally used to gauge the environment, and the robot's appendages can make the appropriate movements. \\\\
I have chosen to use C++ as my programming language, OpenGL as my graphics library, and GLUT as my window manager for this project. The reasons I have chosen these, is mainly due to my familiarity with them already, and that they are the fastest way to render graphics. \\\\
I represent my inverse kinematics model using simple lines, with points at each hinge. The root node is centered at 0,0 in the x,y axis respectively. I refer to the lines as bones in this implementation, and the hinges as joints.
\section{Methodology}
\subsection{Analytical}
Initially, I had 3 bones and 3 joints, and solved for the angles analytically using algebra from the book: \textit{Computer Animation: Algorithms and Techniques} by Rick Parent \cite{parent}. This worked when the number of bones was less than or equal to 3, but not for more, as each angle has to be calculated separately. However, we can generalise this by using cyclic coordinate descent (CCD), explored further on. 
\subsection{Jacobian Pseudo-Inverse}
As we cannot model any number of bones and joints using purely the analytical method, I opted to use the Jacobian pseudo-inverse method to calculate these angles instead, and can now represent an arbitrary amount. This method is also shown by Parent \cite{parent}. We first compose our Jacobian $J$ by creating a matrix of $3 \times n$ size, where $n$ is the amount of the joints. We construct the Jacobian by taking the cross product of the axis we are rotating around ($(0, 0, 1)$ or $z$-axis) and the vector between the end effector (end of the last bone) $E$, and the end of each bone for our x, y, z positions of the bones $P_0 \cdots P_n$, where $P_0$ is the end of our bone system, and $P_n$ is the root bone's end. Thus, it is constructed as such:
\begin{figure}[H]
\begin{equation}
J = 
 \begin{bmatrix}
  (0,0,1) \times E_x & (0,0,1) \times (E-P_1)_x & \cdots & (0,0,1) \times (E-P_n)_x \\
  (0,0,1) \times E_y & (0,0,1) \times (E-P_1)_y & \cdots & (0,0,1) \times (E-P_n)_y\\
  (0,0,1) \times E_z & (0,0,1) \times (E-P_1)_z & \cdots & (0,0,1) \times (E-P_n)_z \\
 \end{bmatrix}
\end{equation}
\label{eq:jacobian}
\end{figure}
Using the Jacobian, we calculate our array of angles $\dot{\theta}$ by doing: 
\begin{figure}[H]
\begin{equation}
\dot{\theta} = J^+V
\end{equation}
\label{eq:theta}
\end{figure}
Where $J^+$ is the pseudo-inverse of the Jacobian. To get our pseudo-inverse, we do:
\begin{figure}[H]
\begin{equation}
J^+ = (J^TJ)^{-1}J^T
\end{equation}
\label{eq:pseudo}
\end{figure}
And $V$ is simply the vector between the goal $G$ and the end effector $E$:
\begin{figure}[H]
\begin{equation}
V = G - E
\end{equation}
\label{eq:V}
\end{figure}
To represent a joint and bone, I created a hierarchical \texttt{Bone} class, which takes the end position of the bone as a parameter. Each bone also keeps track of the parent and child of it, and is treated similarly to a doubly-linked list. It uses the parent the determine the starting position. When we go through a step to calculate the angles for each joint, we first calculate $T = (JJ^T)^{-1}$, and then check the determinant of it. If the determinant is not zero, we know that it is not singular. We then do $TJ^T$ to get the pseudo-inverse of the Jacobian. If the determinant is zero, we simply use $J^T$, as this is still an approximation. The reason we do not use the inverse of the Jacobian directly, is because the amount of joints we have determines the size of the the Jacobian. This means if we have anything other than 3 joints, the matrix won't be square, and we cannot take the inverse of it. After we obtain $\dot{\theta}$, we multiply the angles with our small delta and add them to the current rotation of each joint. We repeat this step until the end bone is close enough to our goal position. The reason we add a small delta is because this method is an approximation to reach our goal, and once each bone has moved, it ceases to be an accurate approximation. \\\\
During the rendering stage, I have used a recursive approach. If the system has yet to reach the goal, we calculate all of the angles for the bones. I then call the render function on the root bone, which recursively calls the render function on its child. Each child uses the previous bone's end position as it's start position, and draw relative from it. This gives us a one-to-one hierarchical bone system.
\subsection{Damped Least Squares Method}
Alongside the pseudo-inverse method, I implemented the damped least squares method, as introduced by Buss \cite{Buss}, first used by Wampler \cite{wampler}. The advantage of this method is that it increases numerical stability from the pseudo-inverse method and guarantees a non-singular matrix (using singular value decomposition (SVD)), such that we do not have to check the determinant. This is calculated as such:
\begin{figure}[H]
\begin{equation}
\dot{\theta} = (J^TJ + \lambda^2I)^{-1}J^TV \label{eq:damp}
\end{equation}
\end{figure}
$\lambda \in \mathbb{R} $ is our non-zero damping constant, which can be dynamically changed depending on the current configuration of the model, but I have chosen a constant, for the sake of simplicity. As can be seen in Equation \ref{eq:damp}, it is very similar to our method of calculating the pseudo-inverse, the only difference being that we add $\lambda^2I'$ to $J^TJ$ before taking the inverse. 
\subsection{Other methods}
We can use SVD to analyse the pseudo-inverse or damp least squares method \cite{Buss}, and derive both of their forms. It is also used to prove that the damp least squares method means we do not get a singular matrix.\\\\
Another method used to calculate the angles is cyclic coordinate descent (CCD) via optimisation \cite{ccd}. In this method, we solve for the sine and cosine of the unknown angle at each joint. This method guarantees convergence, whereas the Jacobian methods do not. Constraints such as stiffness are harder to implement using CCD, however \cite{utah}. 
\section{Task 1}
The first task is to indicate where the end effector of the system can be moved in 2D space. For this, I have simply used a mouse callback function to get a click from the user on the screen, and then transform it to the coordinate system used to render my bones. There is no in-built method in GLUT for converting the x,y position clicked on the screen to the positions used for rendering, so this is done manually. Once I get the position, I use one of the methods implemented to calculate a vector of angles for each one the joints, multiply it by a small delta (dependant on method), so each bone is rotated slightly. This is due to the Jacobian only being an approximation of the rotations to get to the goal, and therefore the process is repeated until the end bone is close enough (0.001) to to the goal. I check the distance between them by calculating the root of the dot product of the points (treated as vectors from the origin).
\section{Task 2}
For this task, we have to incorporate the slow-in, slow-out technique used in animation, where the initial movement is slow, the middle is fast, and the end of the animation is slow. In animation, this is achieved by having more frames at the start and end of the animation than in the middle of it. The Jacobian method naturally takes a fast to slow approach, as it is a convergence process, and as the end bone gets closer to the goal, smaller steps are taken. For this project, I use the distance between the end bone and the goal for this, by having a delay timer, so that it skips every \texttt{x} frames. In this case, \texttt{x} is the distance, until it reaches below 1, in which I set \texttt{x} to a constant, so as to slow it down. Coupled with the gradual slowing down from the Jacobian methods, this works well.
\begin{figure}[H]
\centering
    \subfloat[Against time steps]{{\includegraphics[width=0.4\textwidth]{distance.png}}}
    \qquad
     \subfloat[Against frames]{{\includegraphics[width=0.4\textwidth]{distance2.png}}}
    \caption{Distance plots}
    \label{fig:distance}
\end{figure}
 In Figure \ref{fig:distance}, we plot the distance of the end effector to the goal against the time steps and frames, using the pseudo-inverse method. Every time we calculate the distance from the goal, we wait a corresponding amount of frames before we calculate the angles again. In this instance, a time step is each time we calculate the angles, and a frame is simply each render pass. Here we can see in Figure \ref{fig:distance}(b), the shorter the distance, the less amount of frames it is stationary. And in both graphs, it shows as the end effector gets closer to the goal, the time steps are minimal, and slows down as it converges.
\section{Task 3}
In most bone systems, joints only have a certain amount of freedom to rotate around. Inverse kinematics tries to find the quickest route to the goal, but due to the limitations of how much a joint can be rotated, this is not always possible. We are tasked with modelling the minimum and maximum angles that a bone can move. I have done this simply by adding minimum and maximum angle variables to each of my joints, and clamp the overall angle of the joint between these two after we calculate them at a time step. This ensures it takes a more fluid, and realistic movement towards the goal. However, if the bones have rotated circularly, and the goal requires the bone to unravel, this approach does not work, as the joint tries to rotate towards the goal in the same circular direction it is facing, and cannot reach it, as it has reached its maximum or minimum angle. This can be clearly seen in Figure \ref{fig:wrapped}, as the bones are wrapped in one direction, and cannot unfurl in the other direction to get to the goal (red point). Each of the joints has a -100 to 100 degrees limit. 
\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{wrapped.png}
    \caption{Wrapped around bones}
    \label{fig:wrapped}
\end{figure}
The methods mentioned above try to minimise the distance it takes to get to the goal, and therefore may not have realistic movements. We also want to avoid situations shown in Figure \ref{fig:wrapped}. We can add more control over the direction of the angle for each joint, by adding a control expression, used by Parent \cite{parent}. With this method, we can bias the angles for each joint towards the midpoint angle between the minimum and maximum constraints, and therefore get a more uniform spread over all of the angles when approaching the goal. This is generally known as the \textit{stiffness} of the joints \cite{parent}. This is done by taking the difference from our originally calculated angles using the pseudo-inverse method and the angle we want to achieve. In this case, it is the midpoint angles $\dot{m}$. We then multiply $\dot{m}$ with the gain $\dot{\alpha}$. The gain is how stiff we want each joint to be, in terms of moving away from $\dot{m}$. We can model these soft constraints as:
\begin{figure}[H]
\begin{equation}
\dot{\theta} = J^+V + (J^+J - I)\dot{z}
\end{equation}
\label{eq:stiff}
\end{figure}
Where $\dot{z}$ is an array of the bias towards the midpoint of the minimum and maximum angle, and can be obtained by taking the difference of the angles obtained using the pseudo-inverse $\dot{\theta}$  from the midpoints of the minimum and maximum angles of the joints $\dot{m}$, and multiplying with the amount of gain (or stiffness) of the angles $\dot{\alpha}$ that is wanted, where lower means it will move more towards the edges of the constraints, and higher means that it will bias more towards the midpoint. This is shown below:
\begin{figure}[H]
\begin{equation}
z_i = \alpha_i \cdot (\theta_i - m_i), i =0 .. n-1
\end{equation}
\label{eq:z}
\end{figure}
This gives us a system which is less likely to violate the minimum and maximum angle constraints that we introduced for this task. \\\\
We also want to model the flexibility of the bones, in that one bone can bend slightly to achieve its movement. One option is to introduce several smaller joints along the bone with very small angles of freedom. This, however, is reliant on the method chosen to model the initial angle constraints. I have not had time to try and implement this, but this would allow for more fluid-like movement. As an example, we can think of this approach mimicking a finger, with the main joint being at the base of the finger, and we have two smaller ones along it for added flexibility. Though this is what our current system does, we can incorporate the same idea on a smaller scale within each bone. 
\section{Comparison}
We shall use the quantity of time steps are a comparator for each method, to see the speed at which they converge to reach the same point from the initial start position. We use a value of 0.1 for all of our joints for stiffness, and 0 degrees for all of the midpoint angles. 
\begin{figure}[H]
\centering
\begin{tabular}{| c | c | }
\hline
Method & Time Steps \\ \hline
Pseudo-Inverse & 205 \\ \hline
Pseudo-Inverse with stiffness & 1768 \\ \hline
Damp least squares & 1148 \\ \hline
\end{tabular}
\caption{Comparison of convergence rate between methods}
\label{tab:comp}
\end{figure}
As shown in Figure \ref{tab:comp}, the pseudo-inverse method converges the quickest. This is expected, as the other two methods are based off of the pseudo-inverse, and simply make it move in a more natural way, instead of the fastest way. 
\section{Further Implementation Choices}
Several attributes of the system has been parametrised, to allow for flexibility and generality. All attributes are modified using keys on the keyboard. Firstly, as mentioned before, there is an option to change the method used to calculate the angles. These are pseudo-inverse/transpose of Jacobian, previous method with stiffness, and damped least squares. I have also added in a variable to change the speed at which the systems converges by only calculating the angles every certain amount of frames. We can also add another bone onto the end of the system, which becomes our new end bone. In addition to having changeable properties for the general system, I have also added the option to select and vary the attributes of a particular bone, such as the length of it, the minimum and maximum angles constraints on it, and the stiffness of the particular bone (only used when the additional stiffness method is used).
\section{Conclusion}
There are several solutions to creating a 2D inverse kinematics system, and many of them depend on the context and environment they are used in. We have explored several ways of solving this system, such that we have a realistic approximation of moving a hierarchical set of bones towards a target in 2D space. We also model unrealistic speeds of movement by incorporating slow-in/out, as is a technique commonly used in animation, which gives us smoother transitions from one point to another. I did not, however, implement the system, such that if it cannot physically get to an angle given its current configuration and constraints, it would try to relax some of the other angles, enabling it to find another route to the goal. 
\newpage
\bibliographystyle{plain}
\bibliography{refs}
\end{document}